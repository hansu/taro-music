"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _class, _temp2;

var _index = require("../../../npm/@tarojs/taro-weapp/index.js");

var _index2 = _interopRequireDefault(_index);

var _indexModuleScssMap = require("./index.module.scss.map.js");

var _indexModuleScssMap2 = _interopRequireDefault(_indexModuleScssMap);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var PlayMusicList = (_temp2 = _class = function (_BaseComponent) {
  _inherits(PlayMusicList, _BaseComponent);

  function PlayMusicList() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, PlayMusicList);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = PlayMusicList.__proto__ || Object.getPrototypeOf(PlayMusicList)).call.apply(_ref, [this].concat(args))), _this), _this.$usedState = ["anonymousState__temp", "anonymousState__temp2", "loopArray4", "$compid__25", "$compid__26", "styles", "showPlayer", "musicList", "total", "subscribedCount"], _this.showDetail = function (id) {
      _index2.default.navigateTo({
        url: "/pages/player/index?id=" + id
      });
    }, _this.countFilter = function (val) {
      var hm = val / 100000000;
      var m = val / 10000;
      if (hm > 1) {
        return hm.toFixed(1) + '亿';
      }
      if (m > 1) {
        return parseInt(m) + '万';
      } else {
        return val;
      }
    }, _this.anonymousFunc0Map = {}, _this.customComponents = ["MyIcon", "AtList", "AtListItem"], _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(PlayMusicList, [{
    key: "_constructor",
    value: function _constructor(props) {
      _get(PlayMusicList.prototype.__proto__ || Object.getPrototypeOf(PlayMusicList.prototype), "_constructor", this).call(this, props);
      this.state = {
        showPlayer: false,
        music: []
      };
      this.$$refs = [];
    }
  }, {
    key: "componentWillReceiveProps",
    value: function componentWillReceiveProps(nextProps) {
      console.log(this.props, nextProps);
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {}
  }, {
    key: "_createData",
    value: function _createData() {
      var _this2 = this;

      this.__state = arguments[0] || this.state || {};
      this.__props = arguments[1] || this.props || {};
      var __isRunloopRef = arguments[2];
      var __prefix = this.$prefix;
      ;
      var $compid__25 = (0, _index.genCompid)(__prefix + "$compid__25");
      var $compid__26 = (0, _index.genCompid)(__prefix + "$compid__26");
      var anonymousState__temp = "\u64AD\u653E\u5168\u90E8(" + this.__props.total + "\u9996)";
      var anonymousState__temp2 = "\u6536\u85CF(" + this.__props.subscribedCount + ")";
      var loopArray4 = this.__props.musicList ? this.__props.musicList.map(function (music, index) {
        var _propsManager$set;

        music = {
          $original: (0, _index.internal_get_original)(music)
        };
        var $loopState__temp4 = _this2.__props.musicList ? (0, _index.internal_inline_style)({ textAlign: 'center', width: '10%' }) : null;
        var $loopState__temp6 = _this2.__props.musicList ? (0, _index.internal_inline_style)({ width: '90%' }) : null;

        var _$indexKey = "fzzzz" + index;

        _this2.anonymousFunc0Map[_$indexKey] = function () {
          _this2.showDetail(music.$original.id);
        };

        var $compid__24 = (0, _index.genCompid)(__prefix + "gzzzzzzzzz" + index);
        _index.propsManager.set((_propsManager$set = {
          "arrow": "play",
          "note": music.$original.ar[0].name,
          "title": music.$original.al.name
        }, _defineProperty(_propsManager$set, "arrow", "right"), _defineProperty(_propsManager$set, "onClick", _this2.anonymousFunc0.bind(_this2, _$indexKey)), _propsManager$set), $compid__24);
        return {
          $loopState__temp4: $loopState__temp4,
          $loopState__temp6: $loopState__temp6,
          _$indexKey: _$indexKey,
          $compid__24: $compid__24,
          $original: music.$original
        };
      }) : [];
      _index.propsManager.set({
        "value": "play",
        "size": "15",
        "color": "#000",
        "text": anonymousState__temp
      }, $compid__25);
      _index.propsManager.set({
        "value": "add",
        "size": "15",
        "color": "#fff",
        "text": anonymousState__temp2
      }, $compid__26);
      Object.assign(this.__state, {
        anonymousState__temp: anonymousState__temp,
        anonymousState__temp2: anonymousState__temp2,
        loopArray4: loopArray4,
        $compid__25: $compid__25,
        $compid__26: $compid__26,
        styles: _indexModuleScssMap2.default
      });
      return this.__state;
    }
  }, {
    key: "anonymousFunc0",
    value: function anonymousFunc0(_$indexKey) {
      var _anonymousFunc0Map;

      ;

      for (var _len2 = arguments.length, e = Array(_len2 > 1 ? _len2 - 1 : 0), _key2 = 1; _key2 < _len2; _key2++) {
        e[_key2 - 1] = arguments[_key2];
      }

      return this.anonymousFunc0Map[_$indexKey] && (_anonymousFunc0Map = this.anonymousFunc0Map)[_$indexKey].apply(_anonymousFunc0Map, e);
    }
  }]);

  return PlayMusicList;
}(_index.Component), _class.$$events = [], _class.$$componentPath = "components/content/playMusicList/index", _temp2);
exports.default = PlayMusicList;

Component(require('../../../npm/@tarojs/taro-weapp/index.js').default.createComponent(PlayMusicList));