"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _class, _temp2;

var _index = require("../../../npm/@tarojs/taro-weapp/index.js");

var _index2 = _interopRequireDefault(_index);

var _indexModuleScssMap = require("./index.module.scss.map.js");

var _indexModuleScssMap2 = _interopRequireDefault(_indexModuleScssMap);

var _lodash = require("../../../utils/lodash.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Recommend = (_temp2 = _class = function (_BaseComponent) {
  _inherits(Recommend, _BaseComponent);

  function Recommend() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, Recommend);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = Recommend.__proto__ || Object.getPrototypeOf(Recommend)).call.apply(_ref, [this].concat(args))), _this), _this.$usedState = ["loopArray2", "$compid__15", "$compid__16", "$compid__17", "styles", "title", "start", "status", "showLoadMore", "movieList"], _this.showDetail = function (id) {
      _index2.default.navigateTo({
        url: "/pages/movieDetail/index?id=" + id
      });
    }, _this.anonymousFunc0Map = {}, _this.customComponents = ["AtList", "AtListItem", "AtIcon", "AtLoadMore"], _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(Recommend, [{
    key: "_constructor",
    value: function _constructor(props) {
      _get(Recommend.prototype.__proto__ || Object.getPrototypeOf(Recommend.prototype), "_constructor", this).call(this, props);
      this.state = {
        title: "",
        start: 0,
        status: '',
        showLoadMore: false
      };
      this.$$refs = [];
    }

    // handleClick = () => {
    // }


    // getMoreList = async (api, query) => {
    //     this.setState({
    //         showLoadMore: true,
    //         status: 'loading'
    //     })
    //     let d = await api(query)
    //     this.setState({
    //         movieList: this.state.movieList.concat(d.data.subjects)
    //     }, () => {
    //         if (d.data.subjects.length < 20) {
    //             this.setState({
    //                 status: 'noMore'
    //             })
    //         }
    //         else {
    //             this.setState({
    //                 showLoadMore: false,
    //                 status: ''
    //             })
    //         }
    //     })
    // }

    // loadMore=()=>{
    //     this.setState({
    //         start: this.state.start + 20
    //     }, () => {
    //         let query = {
    //             start: this.state.start,
    //             count: 20
    //         }
    //         this.getMoreList(API.movieModel.GetHotList, query)
    //     })
    // }

  }, {
    key: "componentWillReceiveProps",
    value: function componentWillReceiveProps(nextProps) {
      console.log(this.props, nextProps);
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {}
  }, {
    key: "_createData",
    value: function _createData() {
      var _this2 = this;

      this.__state = arguments[0] || this.state || {};
      this.__props = arguments[1] || this.props || {};
      var __isRunloopRef = arguments[2];
      var __prefix = this.$prefix;
      ;
      var $compid__15 = (0, _index.genCompid)(__prefix + "$compid__15");
      var $compid__16 = (0, _index.genCompid)(__prefix + "$compid__16");
      var $compid__17 = (0, _index.genCompid)(__prefix + "$compid__17");
      var loopArray2 = this.__props.movieList ? this.__props.movieList.map(function (movie, __index0) {
        movie = {
          $original: (0, _index.internal_get_original)(movie)
        };

        var _$indexKey = "dzzzz" + __index0;

        _this2.anonymousFunc0Map[_$indexKey] = function () {
          _this2.showDetail(movie.$original.id);
        };

        var $loopState__temp2 = _this2.__props.movieList ? (0, _index.internal_inline_style)({ fontWeight: "bold", height: '40px' }) : null;
        var $loopState__temp4 = _this2.__props.movieList ? (0, _index.internal_inline_style)({ color: '#fff' }) : null;
        var $loopState__temp6 = _this2.__props.movieList ? (0, _lodash.countFilter)(movie.$original.playCount) : null;
        var $compid__14 = (0, _index.genCompid)(__prefix + "ezzzzzzzzz" + __index0);
        _index.propsManager.set({
          "value": "play",
          "size": "10",
          "color": "#fff"
        }, $compid__14);
        return {
          _$indexKey: _$indexKey,
          $loopState__temp2: $loopState__temp2,
          $loopState__temp4: $loopState__temp4,
          $loopState__temp6: $loopState__temp6,
          $compid__14: $compid__14,
          $original: movie.$original
        };
      }) : [];
      _index.propsManager.set({
        "hasBorder": false
      }, $compid__15);
      _index.propsManager.set({
        "title": "\u63A8\u8350\u6B4C\u5355",
        "hasBorder": false
      }, $compid__16);
      this.__state.showLoadMore && _index.propsManager.set({
        "status": this.__state.status,
        "noMoreText": "\u5DF2\u65E0\u66F4\u591A\u6570\u636E"
      }, $compid__17);
      Object.assign(this.__state, {
        loopArray2: loopArray2,
        $compid__15: $compid__15,
        $compid__16: $compid__16,
        $compid__17: $compid__17,
        styles: _indexModuleScssMap2.default
      });
      return this.__state;
    }
  }, {
    key: "anonymousFunc0",
    value: function anonymousFunc0(_$indexKey) {
      var _anonymousFunc0Map;

      ;

      for (var _len2 = arguments.length, e = Array(_len2 > 1 ? _len2 - 1 : 0), _key2 = 1; _key2 < _len2; _key2++) {
        e[_key2 - 1] = arguments[_key2];
      }

      return this.anonymousFunc0Map[_$indexKey] && (_anonymousFunc0Map = this.anonymousFunc0Map)[_$indexKey].apply(_anonymousFunc0Map, e);
    }
  }]);

  return Recommend;
}(_index.Component), _class.$$events = ["anonymousFunc0"], _class.$$componentPath = "components/common/recommend/index", _temp2);
exports.default = Recommend;

Component(require('../../../npm/@tarojs/taro-weapp/index.js').default.createComponent(Recommend));