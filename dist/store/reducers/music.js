"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _actionTypes = require("../constants/actionTypes.js");

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var INITIAL_STATE = {
  bannerList: [],
  recommendList: []
};

exports.default = function () {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : INITIAL_STATE;
  var action = arguments[1];
  var type = action.type,
      payload = action.payload;

  switch (type) {
    case _actionTypes.SET_BANNERLIST:
      return _extends({}, state, { bannerList: [].concat(_toConsumableArray(payload)) });
    case _actionTypes.SET_RECOMMENDLIST:
      return _extends({}, state, { recommendList: [].concat(_toConsumableArray(payload)) });
    default:
      return state;
  }
};