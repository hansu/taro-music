"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _redux = require("../../npm/redux/lib/redux.js");

var _music = require("./music.js");

var _music2 = _interopRequireDefault(_music);

var _player = require("./player.js");

var _player2 = _interopRequireDefault(_player);

var _user = require("./user.js");

var _user2 = _interopRequireDefault(_user);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = (0, _redux.combineReducers)({
  music: _music2.default,
  player: _player2.default,
  user: _user2.default
});