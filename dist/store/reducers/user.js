"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _actionTypes = require("../constants/actionTypes.js");

var INITIAL_STATE = {
  userInfo: []
};

exports.default = function () {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : INITIAL_STATE;
  var action = arguments[1];
  var type = action.type,
      payload = action.payload;

  switch (type) {
    case _actionTypes.SET_USERINFO:
      return _extends({}, state, { userInfo: _extends({}, payload) });
    default:
      return state;
  }
};