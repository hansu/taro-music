"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.GetRecommendListAction = exports.GetBannerListAction = undefined;

var _index = require("../../apis/index.js");

var _index2 = _interopRequireDefault(_index);

var _actionTypes = require("../constants/actionTypes.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

var GetBannerListAction = exports.GetBannerListAction = function GetBannerListAction() {
  return function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(dispatch) {
      var _ref2, list, action;

      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.next = 2;
              return _index2.default.homeModel.GetBannerList();

            case 2:
              _ref2 = _context.sent;
              list = _ref2.list;

              console.log(list);
              action = {
                type: _actionTypes.SET_BANNERLIST,
                payload: list
              };

              dispatch(action);

            case 7:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, undefined);
    }));

    return function (_x) {
      return _ref.apply(this, arguments);
    };
  }();
};

var GetRecommendListAction = exports.GetRecommendListAction = function GetRecommendListAction() {
  return function () {
    var _ref3 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(dispatch) {
      var _ref4, list, action;

      return regeneratorRuntime.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              _context2.next = 2;
              return _index2.default.homeModel.GetRecommendList();

            case 2:
              _ref4 = _context2.sent;
              list = _ref4.list;
              action = {
                type: _actionTypes.SET_RECOMMENDLIST,
                payload: list
              };

              dispatch(action);

            case 6:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2, undefined);
    }));

    return function (_x2) {
      return _ref3.apply(this, arguments);
    };
  }();
};