"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.GetUserInfoAction = undefined;

var _index = require("../../apis/index.js");

var _index2 = _interopRequireDefault(_index);

var _storage = require("../../utils/helpers/storage.js");

var _storage2 = _interopRequireDefault(_storage);

var _actionTypes = require("../constants/actionTypes.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

var GetUserInfoAction = exports.GetUserInfoAction = function GetUserInfoAction(query) {
  return function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(dispatch) {
      var _ref2, data, action;

      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.next = 2;
              return _index2.default.userModel.GetUserInfo(query);

            case 2:
              _ref2 = _context.sent;
              data = _ref2.data;

              if (data.code == 200) {
                _storage2.default.set("userInfo", data.profile);
              }
              action = {
                type: _actionTypes.SET_USERINFO,
                payload: data.profile
              };

              dispatch(action);
              return _context.abrupt("return", data.code);

            case 8:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, undefined);
    }));

    return function (_x) {
      return _ref.apply(this, arguments);
    };
  }();
};