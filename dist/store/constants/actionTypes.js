"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
var SET_BANNERLIST = exports.SET_BANNERLIST = "SET_BANNERLIST";
var SET_RECOMMENDLIST = exports.SET_RECOMMENDLIST = "SET_RECOMMENDLIST";

var SET_ISPLAY = exports.SET_ISPLAY = "SET_ISPLAY";
var SET_USERINFO = exports.SET_USERINFO = "SET_USERINFO";