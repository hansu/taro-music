"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }(); //函数


var _func = require("../../utils/helpers/func.js");

var _func2 = _interopRequireDefault(_func);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var movieModel = function () {
  function movieModel() {
    _classCallCheck(this, movieModel);
  }

  _createClass(movieModel, [{
    key: "GetHotList",
    value: function GetHotList() {
      var params = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : { start: 0, count: 20 };

      return _func2.default.handleFunc({
        name: 'server',
        data: _extends({
          $url: "movie/getHotList"
        }, params)
      });
    }
  }, {
    key: "GetComingList",
    value: function GetComingList(params) {
      return _func2.default.handleFunc({
        name: 'server',
        data: _extends({
          $url: "movie/getComingList"
        }, params)
      });
    }
  }, {
    key: "GetMovieDetail",
    value: function GetMovieDetail(params) {
      return _func2.default.handleFunc({
        name: 'server',
        data: _extends({
          $url: "movie/getMusicDetail"
        }, params)
      });
    }
  }, {
    key: "GetSongDetail",
    value: function GetSongDetail(params) {
      return _func2.default.handleFunc({
        name: 'server',
        data: _extends({
          $url: "movie/getSongDetail"
        }, params)
      });
    }
  }, {
    key: "GetSongUrl",
    value: function GetSongUrl(params) {
      return _func2.default.handleFunc({
        name: 'server',
        data: _extends({
          $url: "movie/getSongUrl"
        }, params)
      });
    }
  }]);

  return movieModel;
}();

exports.default = new movieModel();