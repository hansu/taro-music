"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }(); //函数


var _func = require("../../utils/helpers/func.js");

var _func2 = _interopRequireDefault(_func);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var homeModel = function () {
  function homeModel() {
    _classCallCheck(this, homeModel);
  }

  _createClass(homeModel, [{
    key: "GetBannerList",
    value: function GetBannerList() {
      return _func2.default.handleFunc({
        name: 'server',
        data: {
          $url: "banner/getList"
        }
      });
    }
  }, {
    key: "GetRecommendList",
    value: function GetRecommendList() {
      return _func2.default.handleFunc({
        name: 'server',
        data: {
          $url: "recommend/getList"
        }
      });
    }
  }]);

  return homeModel;
}();

exports.default = new homeModel();