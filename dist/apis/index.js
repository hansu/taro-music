"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _movie = require("./models/movie.js");

var _movie2 = _interopRequireDefault(_movie);

var _home = require("./models/home.js");

var _home2 = _interopRequireDefault(_home);

var _user = require("./models/user.js");

var _user2 = _interopRequireDefault(_user);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  movieModel: _movie2.default,
  homeModel: _home2.default,
  userModel: _user2.default
};