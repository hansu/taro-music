'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
var countFilter = exports.countFilter = function countFilter(val) {
  var hm = val / 100000000;
  var m = val / 10000;
  if (hm > 1) {
    return hm.toFixed(1) + '亿';
  }
  if (m > 1) {
    return m.toFixed(1) + '万';
  } else {
    return val;
  }
};