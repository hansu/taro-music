"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _dec, _class, _class2, _temp2;

var _index = require("../../npm/@tarojs/taro-weapp/index.js");

var _index2 = _interopRequireDefault(_index);

var _index3 = require("../../npm/@tarojs/redux/index.js");

var _user = require("../../store/actions/user.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Login = (_dec = (0, _index3.connect)(function (_ref) {
  var userInfo = _ref.userInfo;
  return {
    userInfo: userInfo
  };
}, function (dispatch) {
  return {
    asyncGetUserInfo: function asyncGetUserInfo(query) {
      return dispatch((0, _user.GetUserInfoAction)(query));
    }
  };
}), _dec(_class = (_temp2 = _class2 = function (_BaseComponent) {
  _inherits(Login, _BaseComponent);

  function Login() {
    var _ref2,
        _this2 = this;

    var _temp, _this, _ret;

    _classCallCheck(this, Login);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref2 = Login.__proto__ || Object.getPrototypeOf(Login)).call.apply(_ref2, [this].concat(args))), _this), _this.$usedState = ["anonymousState__temp", "$compid__11", "$compid__12", "$compid__13", "phoneValue", "pwdValue", "asyncGetUserInfo"], _this.config = {
      navigationBarTitleText: '登陆'
    }, _this.handlePhoneChange = function (value) {
      _this.setState({
        phoneValue: value
      });
    }, _this.handlePwdChange = function (value) {
      _this.setState({
        pwdValue: value
      });
    }, _this.handleLogin = function () {
      var _ref3 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(phone, password) {
        var query, code;
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                query = {
                  phone: phone,
                  password: password
                };
                _context.next = 3;
                return _this.props.asyncGetUserInfo(query);

              case 3:
                code = _context.sent;

                if (code === 200) {
                  _index2.default.switchTab({ url: "/pages/me/index" });
                }

              case 5:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, _this2);
      }));

      return function (_x, _x2) {
        return _ref3.apply(this, arguments);
      };
    }(), _this.customComponents = ["AtForm", "AtInput"], _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(Login, [{
    key: "_constructor",
    value: function _constructor(props) {
      _get(Login.prototype.__proto__ || Object.getPrototypeOf(Login.prototype), "_constructor", this).call(this, props);
      this.state = {
        phoneValue: '',
        pwdValue: ''
      };
      this.$$refs = [];
    }
  }, {
    key: "onSubmit",
    value: function onSubmit() {
      var _state = this.state,
          phoneValue = _state.phoneValue,
          pwdValue = _state.pwdValue;

      this.handleLogin(phoneValue, pwdValue);
    }
  }, {
    key: "_createData",
    value: function _createData() {
      this.__state = arguments[0] || this.state || {};
      this.__props = arguments[1] || this.props || {};
      var __isRunloopRef = arguments[2];
      var __prefix = this.$prefix;
      ;
      var $compid__11 = (0, _index.genCompid)(__prefix + "$compid__11");
      var $compid__12 = (0, _index.genCompid)(__prefix + "$compid__12");
      var $compid__13 = (0, _index.genCompid)(__prefix + "$compid__13");
      var anonymousState__temp = (0, _index.internal_inline_style)({ border: 'none', borderRadius: '100px', marginTop: "10px" });
      _index.propsManager.set({
        "onSubmit": this.onSubmit.bind(this)
      }, $compid__11);
      _index.propsManager.set({
        "name": "value",
        "title": "\u624B\u673A\u53F7",
        "type": "text",
        "placeholder": "\u8F93\u5165\u624B\u673A\u53F7",
        "value": this.__state.phoneValue,
        "onChange": this.handlePhoneChange
      }, $compid__12);
      _index.propsManager.set({
        "name": "value",
        "title": "\u5BC6\u7801",
        "type": "password",
        "placeholder": "\u8F93\u5165\u5BC6\u7801",
        "value": this.__state.pwdValue,
        "onChange": this.handlePwdChange
      }, $compid__13);
      Object.assign(this.__state, {
        anonymousState__temp: anonymousState__temp,
        $compid__11: $compid__11,
        $compid__12: $compid__12,
        $compid__13: $compid__13
      });
      return this.__state;
    }
  }]);

  return Login;
}(_index.Component), _class2.$$events = [], _class2.$$componentPath = "pages/login/index", _temp2)) || _class);
exports.default = Login;

Component(require('../../npm/@tarojs/taro-weapp/index.js').default.createComponent(Login, true));