"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _class, _temp2;

var _index = require("../../../../npm/@tarojs/taro-weapp/index.js");

var _index2 = _interopRequireDefault(_index);

var _storage = require("../../../../utils/helpers/storage.js");

var _storage2 = _interopRequireDefault(_storage);

var _meModuleScssMap = require("./me.module.scss.map.js");

var _meModuleScssMap2 = _interopRequireDefault(_meModuleScssMap);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Me = (_temp2 = _class = function (_BaseComponent) {
  _inherits(Me, _BaseComponent);

  function Me() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, Me);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = Me.__proto__ || Object.getPrototypeOf(Me)).call.apply(_ref, [this].concat(args))), _this), _this.$usedState = ["anonymousState__temp", "anonymousState__temp2", "anonymousState__temp3", "$compid__18", "$compid__19", "$compid__20", "$compid__21", "styles", "userInfo", "token", "isOpened"], _this.config = {
      navigationBarTitleText: '个人中心'
    }, _this.state = {
      userInfo: _storage2.default.get('userInfo') || {},
      token: _storage2.default.get('token') || '',
      isOpened: false
    }, _this.handleCancel = function () {
      _this.setState({
        isOpened: false
      });
    }, _this.customComponents = ["AtAvatar", "AtIcon", "AtList", "AtListItem"], _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(Me, [{
    key: "_constructor",
    value: function _constructor(props) {
      _get(Me.prototype.__proto__ || Object.getPrototypeOf(Me.prototype), "_constructor", this).call(this, props);

      this.$$refs = [];
    }
  }, {
    key: "componentDidShow",
    value: function componentDidShow() {
      console.log(_storage2.default.get('userInfo'));
    }
  }, {
    key: "_createData",
    value: function _createData() {
      this.__state = arguments[0] || this.state || {};
      this.__props = arguments[1] || this.props || {};
      var __isRunloopRef = arguments[2];
      var __prefix = this.$prefix;
      ;
      var $compid__18 = (0, _index.genCompid)(__prefix + "$compid__18");
      var $compid__19 = (0, _index.genCompid)(__prefix + "$compid__19");
      var $compid__20 = (0, _index.genCompid)(__prefix + "$compid__20");
      var $compid__21 = (0, _index.genCompid)(__prefix + "$compid__21");

      var mewrapperStyle = {
        position: "absolute",
        top: "0",
        bottom: "0",
        left: "0",
        right: "0",
        background: 'url(' + this.__state.userInfo.avatarUrl + ')',
        backgroundSize: 'cover',
        filter: "blur(10px)",
        zIndex: "-2"
      };
      var anonymousState__temp = (0, _index.internal_inline_style)(mewrapperStyle);
      var anonymousState__temp2 = {
        size: 25, value: 'star'
      };
      var anonymousState__temp3 = {
        size: 25, value: 'sound'
      };
      _index.propsManager.set({
        "circle": true,
        "image": this.__state.userInfo.avatarUrl
      }, $compid__18);
      _index.propsManager.set({
        "value": "edit"
      }, $compid__19);
      _index.propsManager.set({
        "title": "\u6211\u7684\u6536\u85CF",
        "arrow": "right",
        "iconInfo": anonymousState__temp2
      }, $compid__20);
      _index.propsManager.set({
        "title": "\u6211\u7684\u7535\u53F0",
        "arrow": "right",
        "iconInfo": anonymousState__temp3
      }, $compid__21);
      Object.assign(this.__state, {
        anonymousState__temp: anonymousState__temp,
        anonymousState__temp2: anonymousState__temp2,
        anonymousState__temp3: anonymousState__temp3,
        $compid__18: $compid__18,
        $compid__19: $compid__19,
        $compid__20: $compid__20,
        $compid__21: $compid__21,
        styles: _meModuleScssMap2.default
      });
      return this.__state;
    }
  }]);

  return Me;
}(_index.Component), _class.$$events = [], _class.$$componentPath = "pages/me/components/meInfo/index", _temp2);
exports.default = Me;

Component(require('../../../../npm/@tarojs/taro-weapp/index.js').default.createComponent(Me));