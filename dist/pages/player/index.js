"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _class, _temp2;

var _index = require("../../npm/@tarojs/taro-weapp/index.js");

var _index2 = _interopRequireDefault(_index);

var _indexModuleScssMap = require("./index.module.scss.map.js");

var _indexModuleScssMap2 = _interopRequireDefault(_indexModuleScssMap);

var _index3 = require("../../apis/index.js");

var _index4 = _interopRequireDefault(_index3);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var BAM = _index2.default.getBackgroundAudioManager();

var MyPlayer = (_temp2 = _class = function (_BaseComponent) {
  _inherits(MyPlayer, _BaseComponent);

  function MyPlayer() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, MyPlayer);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = MyPlayer.__proto__ || Object.getPrototypeOf(MyPlayer)).call.apply(_ref, [this].concat(args))), _this), _this.$usedState = ["anonymousState__temp", "anonymousState__temp2", "$compid__10", "styles", "music", "isCurPlay", "playIcon"], _this.config = {
      navigationBarTitleText: '歌单'
    }, _this.handlePlay = function () {
      if (!_this.state.isCurPlay) {
        if (_this.state.musicUrl) {
          _this.setState({
            isCurPlay: true,
            playIcon: 'pause'
          }, function () {
            if (_this.state.musicUrl) {
              BAM.src = _this.state.musicUrl;
              BAM.title = _this.state.music.name;
              BAM.singer = _this.state.music.al.name;
              BAM.coverImgUrl = _this.state.music.picUrl;
              BAM.play();
            }
          });
        } else {
          _index2.default.showToast({
            title: "需要解锁会员权限",
            icon: "none"
          });
        }
      } else {
        _this.setState({
          isCurPlay: false,
          playIcon: 'play'
        }, function () {
          BAM.pause();
        });
      }
    }, _this.customComponents = ["MyIcon"], _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(MyPlayer, [{
    key: "_constructor",
    value: function _constructor(props) {
      _get(MyPlayer.prototype.__proto__ || Object.getPrototypeOf(MyPlayer.prototype), "_constructor", this).call(this, props);
      this.state = {
        isCurPlay: false,
        music: {},
        playIcon: 'play'
      };
      this.$$refs = [];
    }
  }, {
    key: "componentWillMount",
    value: function () {
      var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
        var _this2 = this;

        var id, _ref3, songs, _ref4, data;

        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                id = this.$router.params.id;
                _context.next = 3;
                return _index4.default.movieModel.GetSongDetail({ id: id });

              case 3:
                _ref3 = _context.sent;
                songs = _ref3.data.songs;
                _context.next = 7;
                return _index4.default.movieModel.GetSongUrl({ id: id });

              case 7:
                _ref4 = _context.sent;
                data = _ref4.data;

                this.setState({
                  music: songs[0],
                  musicUrl: data[0].url
                }, function () {
                  _index2.default.setNavigationBarTitle({
                    title: _this2.state.music.al.name
                  });
                  _this2.handlePlay();
                  BAM.onPlay(function () {
                    console.log("play");
                  });
                });

              case 10:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function componentWillMount() {
        return _ref2.apply(this, arguments);
      }

      return componentWillMount;
    }()
  }, {
    key: "_createData",
    value: function _createData() {
      this.__state = arguments[0] || this.state || {};
      this.__props = arguments[1] || this.props || {};
      var __isRunloopRef = arguments[2];
      var __prefix = this.$prefix;
      ;
      var $compid__10 = (0, _index.genCompid)(__prefix + "$compid__10");

      var song_disc = {};
      if (Object.keys(this.__state.music).length) {
        song_disc = {
          background: 'url(' + this.__state.music.al.picUrl + ')',
          backgroundSize: 'cover',
          position: "absolute",
          top: 0,
          bottom: 0,
          left: 0,
          right: 0,
          zIndex: -1,
          backgroundColor: '#161824',
          backgroundPosition: '50%'
        };
      }
      var anonymousState__temp = Object.keys(this.__state.music).length ? (0, _index.internal_inline_style)(song_disc) : null;
      var anonymousState__temp2 = Object.keys(this.__state.music).length;
      anonymousState__temp2 && _index.propsManager.set({
        "value": this.__state.playIcon,
        "size": "30",
        "color": "#fff"
      }, $compid__10);
      Object.assign(this.__state, {
        anonymousState__temp: anonymousState__temp,
        anonymousState__temp2: anonymousState__temp2,
        $compid__10: $compid__10,
        styles: _indexModuleScssMap2.default
      });
      return this.__state;
    }
  }]);

  return MyPlayer;
}(_index.Component), _class.$$events = ["handlePlay"], _class.$$componentPath = "pages/player/index", _temp2);
exports.default = MyPlayer;

Component(require('../../npm/@tarojs/taro-weapp/index.js').default.createComponent(MyPlayer, true));