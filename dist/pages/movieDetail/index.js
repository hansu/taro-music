"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _class, _temp2;

var _index = require("../../npm/@tarojs/taro-weapp/index.js");

var _index2 = _interopRequireDefault(_index);

var _index3 = require("../../apis/index.js");

var _index4 = _interopRequireDefault(_index3);

var _lodash = require("../../utils/lodash.js");

var _indexModuleScssMap = require("./index.module.scss.map.js");

var _indexModuleScssMap2 = _interopRequireDefault(_indexModuleScssMap);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var movieDetail = (_temp2 = _class = function (_BaseComponent) {
  _inherits(movieDetail, _BaseComponent);

  function movieDetail() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, movieDetail);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = movieDetail.__proto__ || Object.getPrototypeOf(movieDetail)).call.apply(_ref, [this].concat(args))), _this), _this.$usedState = ["anonymousState__temp", "anonymousState__temp2", "anonymousState__temp3", "anonymousState__temp4", "anonymousState__temp5", "anonymousState__temp6", "$compid__9", "styles", "musicInfo"], _this.config = {
      navigationBarTitleText: '歌单'
    }, _this.customComponents = ["PlayMusicList"], _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(movieDetail, [{
    key: "_constructor",
    value: function _constructor(props) {
      _get(movieDetail.prototype.__proto__ || Object.getPrototypeOf(movieDetail.prototype), "_constructor", this).call(this, props);
      this.state = {
        musicInfo: {}
      };
      this.$$refs = [];
    }
  }, {
    key: "componentDidMount",
    value: function () {
      var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
        var query, _ref3, data;

        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                query = {
                  id: this.$router.params.id
                  // id:32659890
                };
                _context.next = 3;
                return _index4.default.movieModel.GetMovieDetail(query);

              case 3:
                _ref3 = _context.sent;
                data = _ref3.data;

                console.log(data.playlist);
                this.setState({
                  musicInfo: data.playlist
                });

              case 7:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function componentDidMount() {
        return _ref2.apply(this, arguments);
      }

      return componentDidMount;
    }()
  }, {
    key: "componentWillReceiveProps",
    value: function componentWillReceiveProps(nextProps) {
      console.log(this.props, nextProps);
    }
  }, {
    key: "_createData",
    value: function _createData() {
      this.__state = arguments[0] || this.state || {};
      this.__props = arguments[1] || this.props || {};
      var __isRunloopRef = arguments[2];
      var __prefix = this.$prefix;
      ;
      var $compid__9 = (0, _index.genCompid)(__prefix + "$compid__9");

      var musicInfo = this.__state.musicInfo;

      if (musicInfo.coverImgUrl) {
        var headStyle = {
          background: "url(" + musicInfo.coverImgUrl + ")",
          zIndex: '-2',
          position: 'absolute',
          top: 0,
          left: 0,
          right: 0,
          bottom: 0,
          filter: 'blur(20px)'
        };
      }
      var anonymousState__temp = Object.keys(musicInfo).length ? (0, _index.internal_inline_style)(headStyle) : null;
      var anonymousState__temp2 = Object.keys(musicInfo).length ? (0, _index.internal_inline_style)({ fontSize: "16px" }) : null;
      var anonymousState__temp3 = Object.keys(musicInfo).length ? (0, _index.internal_inline_style)({ fontSize: "14px", marginLeft: "10px" }) : null;
      var anonymousState__temp4 = Object.keys(musicInfo).length ? (0, _index.internal_inline_style)({ borderRadius: "100px" }) : null;
      var anonymousState__temp5 = Object.keys(musicInfo).length ? (0, _lodash.countFilter)(musicInfo.subscribedCount) : null;
      var anonymousState__temp6 = Object.keys(musicInfo).length;
      anonymousState__temp6 && _index.propsManager.set({
        "musicList": musicInfo.tracks,
        "total": musicInfo.tracks.length,
        "subscribedCount": anonymousState__temp5
      }, $compid__9);
      Object.assign(this.__state, {
        anonymousState__temp: anonymousState__temp,
        anonymousState__temp2: anonymousState__temp2,
        anonymousState__temp3: anonymousState__temp3,
        anonymousState__temp4: anonymousState__temp4,
        anonymousState__temp5: anonymousState__temp5,
        anonymousState__temp6: anonymousState__temp6,
        $compid__9: $compid__9,
        styles: _indexModuleScssMap2.default
      });
      return this.__state;
    }
  }]);

  return movieDetail;
}(_index.Component), _class.$$events = [], _class.$$componentPath = "pages/movieDetail/index", _temp2);
exports.default = movieDetail;

Component(require('../../npm/@tarojs/taro-weapp/index.js').default.createComponent(movieDetail, true));