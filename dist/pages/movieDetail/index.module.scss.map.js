module.exports = {
  "detail_wrapper": "index-module__detail_wrapper___2Hn1g",
  "detail_container": "index-module__detail_container___8U4sA",
  "detail_header": "index-module__detail_header___J5q-Y",
  "detail_header_info": "index-module__detail_header_info___u6xF0",
  "sub_info_cover": "index-module__sub_info_cover___19fwK",
  "sub_info_content": "index-module__sub_info_content___gSEO2",
  "sub_info_creator": "index-module__sub_info_creator___1MZI9",
  "sub_info_mark": "index-module__sub_info_mark___26SNl",
  "sub_info_mark_btn": "index-module__sub_info_mark_btn___Rjj6g",
  "detail_summary": "index-module__detail_summary___moYfb"
};
