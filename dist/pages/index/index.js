"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _dec, _class, _class2, _temp2;

// import MovieList from "./components/movieList"


var _index = require("../../npm/@tarojs/taro-weapp/index.js");

var _index2 = _interopRequireDefault(_index);

var _index3 = require("../../npm/@tarojs/redux/index.js");

var _music = require("../../store/actions/music.js");

var _homeModuleScssMap = require("./home.module.scss.map.js");

var _homeModuleScssMap2 = _interopRequireDefault(_homeModuleScssMap);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Index = (_dec = (0, _index3.connect)(function (_ref) {
  var _ref$music = _ref.music,
      bannerList = _ref$music.bannerList,
      recommendList = _ref$music.recommendList;
  return {
    bannerList: bannerList,
    recommendList: recommendList
  };
}, function (dispatch) {
  return {
    asyncGetBannerList: function asyncGetBannerList() {
      dispatch((0, _music.GetBannerListAction)());
    },
    asyncGetRecommendList: function asyncGetRecommendList() {
      dispatch((0, _music.GetRecommendListAction)());
    }
  };
}), _dec(_class = (_temp2 = _class2 = function (_BaseComponent) {
  _inherits(Index, _BaseComponent);

  function Index() {
    var _ref2;

    var _temp, _this, _ret;

    _classCallCheck(this, Index);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref2 = Index.__proto__ || Object.getPrototypeOf(Index)).call.apply(_ref2, [this].concat(args))), _this), _this.$usedState = ["anonymousState__temp", "anonymousState__temp2", "anonymousState__temp3", "anonymousState__temp4", "anonymousState__temp5", "$compid__0", "$compid__1", "$compid__2", "$compid__3", "$compid__4", "$compid__5", "$compid__6", "styles", "bannerList", "asyncGetBannerList", "recommendList", "asyncGetRecommendList"], _this.config = {
      navigationBarTitleText: '网易云音乐'
    }, _this.showDetail = function (type) {
      switch (type) {
        case "4":
          _index2.default.navigateTo({ url: '/pages/rank/index' });
      }
    }, _this.customComponents = ["MyBanner", "AtIcon", "MyRecommend"], _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(Index, [{
    key: "_constructor",
    value: function _constructor(props) {
      _get(Index.prototype.__proto__ || Object.getPrototypeOf(Index.prototype), "_constructor", this).call(this, props);
      this.$$refs = [];
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {}
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {
      if (!this.props.bannerList.length) {
        this.props.asyncGetBannerList();
      }
      if (!this.props.recommendList.length) {
        this.props.asyncGetRecommendList();
      }
    }
  }, {
    key: "_createData",
    value: function _createData() {
      var _this2 = this;

      this.__state = arguments[0] || this.state || {};
      this.__props = arguments[1] || this.props || {};
      var __isRunloopRef = arguments[2];
      var __prefix = this.$prefix;
      ;
      var $compid__0 = (0, _index.genCompid)(__prefix + "$compid__0");
      var $compid__1 = (0, _index.genCompid)(__prefix + "$compid__1");
      var $compid__2 = (0, _index.genCompid)(__prefix + "$compid__2");
      var $compid__3 = (0, _index.genCompid)(__prefix + "$compid__3");
      var $compid__4 = (0, _index.genCompid)(__prefix + "$compid__4");
      var $compid__5 = (0, _index.genCompid)(__prefix + "$compid__5");
      var $compid__6 = (0, _index.genCompid)(__prefix + "$compid__6");
      var anonymousState__temp = (0, _index.internal_inline_style)({ marginTop: "10px" });
      var anonymousState__temp2 = (0, _index.internal_inline_style)({ marginTop: "10px" });
      var anonymousState__temp3 = (0, _index.internal_inline_style)({ marginTop: "10px" });

      this.anonymousFunc0 = function () {
        _this2.showDetail("4");
      };

      var anonymousState__temp4 = (0, _index.internal_inline_style)({ marginTop: "10px" });
      var anonymousState__temp5 = (0, _index.internal_inline_style)({ marginTop: "10px" });
      _index.propsManager.set({
        "list": this.__props.bannerList
      }, $compid__0);
      _index.propsManager.set({
        "value": "calendar",
        "color": "#fff"
      }, $compid__1);
      _index.propsManager.set({
        "value": "file-audio",
        "color": "#fff"
      }, $compid__2);
      _index.propsManager.set({
        "value": "sound",
        "color": "#fff"
      }, $compid__3);
      _index.propsManager.set({
        "value": "align-center",
        "color": "#fff"
      }, $compid__4);
      _index.propsManager.set({
        "value": "video",
        "color": "#fff"
      }, $compid__5);
      _index.propsManager.set({
        "movieList": this.__props.recommendList
      }, $compid__6);
      Object.assign(this.__state, {
        anonymousState__temp: anonymousState__temp,
        anonymousState__temp2: anonymousState__temp2,
        anonymousState__temp3: anonymousState__temp3,
        anonymousState__temp4: anonymousState__temp4,
        anonymousState__temp5: anonymousState__temp5,
        $compid__0: $compid__0,
        $compid__1: $compid__1,
        $compid__2: $compid__2,
        $compid__3: $compid__3,
        $compid__4: $compid__4,
        $compid__5: $compid__5,
        $compid__6: $compid__6,
        styles: _homeModuleScssMap2.default
      });
      return this.__state;
    }
  }, {
    key: "anonymousFunc0",
    value: function anonymousFunc0(e) {
      ;
    }
  }]);

  return Index;
}(_index.Component), _class2.$$events = ["anonymousFunc0"], _class2.$$componentPath = "pages/index/index", _temp2)) || _class);
exports.default = Index;

Component(require('../../npm/@tarojs/taro-weapp/index.js').default.createComponent(Index, true));