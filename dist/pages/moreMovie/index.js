"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _class, _temp2;

var _index = require("../../npm/@tarojs/taro-weapp/index.js");

var _index2 = _interopRequireDefault(_index);

var _index3 = require("../../apis/index.js");

var _index4 = _interopRequireDefault(_index3);

var _moreModuleScssMap = require("./more.module.scss.map.js");

var _moreModuleScssMap2 = _interopRequireDefault(_moreModuleScssMap);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var moreMovie = (_temp2 = _class = function (_BaseComponent) {
  _inherits(moreMovie, _BaseComponent);

  function moreMovie() {
    var _ref,
        _this2 = this;

    var _temp, _this, _ret;

    _classCallCheck(this, moreMovie);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = moreMovie.__proto__ || Object.getPrototypeOf(moreMovie)).call.apply(_ref, [this].concat(args))), _this), _this.$usedState = ["loopArray0", "$compid__8", "styles", "title", "movieList", "start", "status", "showLoadMore"], _this.config = {
      navigationBarTitleText: '豆瓣电影'
    }, _this.handleClick = function () {}, _this.getList = function () {
      var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(api) {
        var d;
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _index2.default.showLoading({
                  title: 'loading'
                });
                _context.next = 3;
                return api();

              case 3:
                d = _context.sent;

                _this.setState({
                  movieList: d.data.subjects
                }, function () {
                  _index2.default.hideLoading();
                });

              case 5:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, _this2);
      }));

      return function (_x) {
        return _ref2.apply(this, arguments);
      };
    }(), _this.getMoreList = function () {
      var _ref3 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(api, query) {
        var d;
        return regeneratorRuntime.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _this.setState({
                  showLoadMore: true,
                  status: 'loading'
                });
                _context2.next = 3;
                return api(query);

              case 3:
                d = _context2.sent;

                _this.setState({
                  movieList: _this.state.movieList.concat(d.data.subjects)
                }, function () {
                  if (d.data.subjects.length < 20) {
                    _this.setState({
                      status: 'noMore'
                    });
                  } else {
                    _this.setState({
                      showLoadMore: false,
                      status: ''
                    });
                  }
                });

              case 5:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, _this2);
      }));

      return function (_x2, _x3) {
        return _ref3.apply(this, arguments);
      };
    }(), _this.loadMore = function () {
      _this.setState({
        start: _this.state.start + 20
      }, function () {
        var query = {
          start: _this.state.start,
          count: 20
        };
        _this.getMoreList(_index4.default.movieModel.GetHotList, query);
      });
    }, _this.showDetail = function (id) {
      _index2.default.navigateTo({
        url: "/pages/movieDetail/index?id=" + id
      });
    }, _this.anonymousFunc0Map = {}, _this.customComponents = ["AtRate", "AtLoadMore"], _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(moreMovie, [{
    key: "_constructor",
    value: function _constructor(props) {
      _get(moreMovie.prototype.__proto__ || Object.getPrototypeOf(moreMovie.prototype), "_constructor", this).call(this, props);
      this.state = {
        title: "",
        movieList: [],
        start: 0,
        status: '',
        showLoadMore: false
      };
      this.$$refs = [];
    }
  }, {
    key: "componentWillMount",
    value: function componentWillMount() {
      var _this3 = this;

      this.setState({
        title: this.$router.params.title
      }, function () {
        switch (_this3.state.title) {
          case "正在热映":
            _this3.getList(_index4.default.movieModel.GetHotList);break;
        }
      });
    }
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {
      _index2.default.setNavigationBarTitle({
        title: this.state.title
      });
    }
  }, {
    key: "componentWillReceiveProps",
    value: function componentWillReceiveProps(nextProps) {
      console.log(this.props, nextProps);
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {}
  }, {
    key: "onReachBottom",
    value: function onReachBottom() {
      this.state.status !== 'noMore' && this.loadMore();
    }
  }, {
    key: "_createData",
    value: function _createData() {
      var _this4 = this;

      this.__state = arguments[0] || this.state || {};
      this.__props = arguments[1] || this.props || {};
      var __isRunloopRef = arguments[2];
      var __prefix = this.$prefix;
      ;
      var $compid__8 = (0, _index.genCompid)(__prefix + "$compid__8");
      var loopArray0 = this.__state.movieList ? this.__state.movieList.map(function (movie, __index0) {
        movie = {
          $original: (0, _index.internal_get_original)(movie)
        };

        var _$indexKey = "azzzz" + __index0;

        _this4.anonymousFunc0Map[_$indexKey] = function () {
          _this4.showDetail(movie.$original.id);
        };

        var $loopState__temp2 = movie.$original.rating.average ? movie.$original.rating.average / 2 : null;
        var $loopState__temp4 = movie.$original.rating.average ? movie.$original.rating.average.toFixed(1) : null;
        var $loopState__temp6 = _this4.__state.movieList ? (0, _index.internal_inline_style)({ fontWeight: "bold" }) : null;
        var $compid__7 = (0, _index.genCompid)(__prefix + "bzzzzzzzzz" + __index0);
        movie.$original.rating.average && _index.propsManager.set({
          "size": "10",
          "value": $loopState__temp2
        }, $compid__7);
        return {
          _$indexKey: _$indexKey,
          $loopState__temp2: $loopState__temp2,
          $loopState__temp4: $loopState__temp4,
          $loopState__temp6: $loopState__temp6,
          $compid__7: $compid__7,
          $original: movie.$original
        };
      }) : [];
      this.__state.showLoadMore && _index.propsManager.set({
        "status": this.__state.status,
        "noMoreText": "\u5DF2\u65E0\u66F4\u591A\u6570\u636E"
      }, $compid__8);
      Object.assign(this.__state, {
        loopArray0: loopArray0,
        $compid__8: $compid__8,
        styles: _moreModuleScssMap2.default
      });
      return this.__state;
    }
  }, {
    key: "anonymousFunc0",
    value: function anonymousFunc0(_$indexKey) {
      var _anonymousFunc0Map;

      ;

      for (var _len2 = arguments.length, e = Array(_len2 > 1 ? _len2 - 1 : 0), _key2 = 1; _key2 < _len2; _key2++) {
        e[_key2 - 1] = arguments[_key2];
      }

      return this.anonymousFunc0Map[_$indexKey] && (_anonymousFunc0Map = this.anonymousFunc0Map)[_$indexKey].apply(_anonymousFunc0Map, e);
    }
  }]);

  return moreMovie;
}(_index.Component), _class.$$events = ["anonymousFunc0"], _class.$$componentPath = "pages/moreMovie/index", _temp2);
exports.default = moreMovie;

Component(require('../../npm/@tarojs/taro-weapp/index.js').default.createComponent(moreMovie, true));