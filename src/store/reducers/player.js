import { SET_ISPLAY } from '../constants/actionTypes'

const INITIAL_STATE = {
   isPlay:false
}

export default (state = INITIAL_STATE, action) => {
  let { type } = action
  switch (type) {
    case SET_ISPLAY:
    return {...state, isPlay:!state.isPlay}
    default:
      return state
  }
}
