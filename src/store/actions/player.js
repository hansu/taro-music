import API from "@/apis"
import {SET_ISPLAY} from "@/store/constants/actionTypes"
export const SetIsPlayAction=(isPlay=false)=>{
    return dispatch=>{
        let action={
            type:SET_ISPLAY,
            payload:isPlay
        }
        dispatch(action)
    }
}
