import API from "@/apis"
import {SET_BANNERLIST,SET_RECOMMENDLIST} from "@/store/constants/actionTypes"
export const GetBannerListAction=()=>{
    return async dispatch=>{
        let {list}=await API.homeModel.GetBannerList()
        console.log(list)
        let action={
            type:SET_BANNERLIST,
            payload:list
        }
        dispatch(action)
    }
}

export const GetRecommendListAction=()=>{
    return async dispatch=>{
        let {list}=await API.homeModel.GetRecommendList()
        let action={
            type:SET_RECOMMENDLIST,
            payload:list
        }
        dispatch(action)
    }
}