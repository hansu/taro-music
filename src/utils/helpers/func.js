class wxFunction {
    handleFunc(config){
        return new Promise((resolve,reject)=>{
            wx.cloud.callFunction({
                ...config,
                success: res=>{
                  resolve(res.result)
                },
                fail: error=>{
                    reject(error)
                }
            })
        }) 
    }
}
export default new wxFunction()