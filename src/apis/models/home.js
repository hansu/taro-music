//函数
import wxFunc from "../../utils/helpers/func"
class homeModel {
    GetBannerList() {
        return wxFunc.handleFunc({
            name: 'server', 
            data: {
                $url:"banner/getList"
            }
        })
    }

    GetRecommendList() {
        return wxFunc.handleFunc({
            name: 'server', 
            data: {
                $url:"recommend/getList"
            }
        })
    }
}
export default new homeModel()