//函数
import wxFunc from "../../utils/helpers/func"
class movieModel {
    GetHotList(params={start:0,count:20}) {
        return wxFunc.handleFunc({
            name: 'server', 
            data: {
                $url:"movie/getHotList",
                ...params
            }
        })
    }
    GetComingList(params) {
        return wxFunc.handleFunc({
            name: 'server', 
            data: {
                $url:"movie/getComingList",
                ...params
            }
        })
    }
    GetMovieDetail(params) {
        return wxFunc.handleFunc({
            name: 'server', 
            data: {
                $url:"movie/getMusicDetail",
                ...params
            }
        })
    }
    GetSongDetail(params) {
        return wxFunc.handleFunc({
            name: 'server', 
            data: {
                $url:"movie/getSongDetail",
                ...params
            }
        })
    }
    GetSongUrl(params) {
        return wxFunc.handleFunc({
            name: 'server', 
            data: {
                $url:"movie/getSongUrl",
                ...params
            }
        })
    }
}
export default new movieModel()