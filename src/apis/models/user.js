//函数
import wxFunc from "../../utils/helpers/func"
class userModel {
    GetUserInfo(query) {
        return wxFunc.handleFunc({
            name: 'server', 
            data: {
                $url:"login/getToken",
                ...query
            }
        })
    }

}
export default new userModel()