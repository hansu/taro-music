import movieModel from "./models/movie"
import homeModel from "./models/home"
import userModel from "./models/user"
export default {
    movieModel,
    homeModel,
    userModel
}