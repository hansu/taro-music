import Taro, { Component } from '@tarojs/taro'
import { View, Text } from '@tarojs/components'
import { AtLoadMore, AtList, AtListItem } from "taro-ui"
import MyIcon from "@/components/common/myIcon"
import styles from "./index.module.scss";
class PlayMusicList extends Component {

    constructor(props) {
        super(props)
        this.state = {
            showPlayer:false,
            music:[]
        }
    }


    showDetail = (id) => {
       Taro.navigateTo({
           url:`/pages/player/index?id=${id}`,
       })
    }

    countFilter = (val) => {
        let hm = val / 100000000
        let m = val / 10000
        if (hm > 1) {
            return hm.toFixed(1) + '亿'
        }
        if (m > 1) {
            return parseInt(m) + '万'
        }
        else {
            return val
        }
    }

    componentWillReceiveProps(nextProps) {
        console.log(this.props, nextProps)
    }

    componentWillUnmount() { }

    render() {
        return (
            <View className={styles.list_wrapper}>
                <View className={styles.list_header}>
                    <View className={styles.list_header_left}>
                    <MyIcon value="play" size="15" color="#000" text={`播放全部(${this.props.total}首)`}></MyIcon>
                    </View>
                    <View className={styles.list_header_right}>
                    <MyIcon value="add" size="15" color="#fff" text={`收藏(${this.props.subscribedCount})`}></MyIcon>
                    </View>
                </View>
                <View>
                    <AtList>
                        {this.props.musicList ? this.props.musicList.map((music, index) => {
                            return (
                                <View className={styles.music_item_container} key={music.id}>
                                    <View style={{ textAlign: 'center', width: '10%' }}>{index + 1}</View>
                                    <View style={{ width: '90%' }}>
                                        <AtListItem
                                            arrow='play'
                                            note={music.ar[0].name}
                                            title={music.al.name}
                                            key={music.id}
                                            arrow="right"
                                            onClick={() => { this.showDetail(music.id) }}
                                        />
                                    </View>
                                </View>

                            )
                        }) : "暂无数据"}
                    </AtList>
                </View>
                <View>
                    {/* {
                        this.state.showLoadMore ? <AtLoadMore status={this.state.status} noMoreText='已无更多数据' /> : ''
                    } */}
                </View>
            </View>
        )
    }
}

export default PlayMusicList
