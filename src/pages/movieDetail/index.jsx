import Taro, { Component } from '@tarojs/taro'
import { View, Text } from '@tarojs/components'
import PlayMusicList from "@/components/content/playMusicList"
import API from "@/apis"
import { countFilter } from "@/utils/lodash"
import styles from "./index.module.scss";


class movieDetail extends Component {

    constructor(props) {
        super(props)
        this.state = {
            musicInfo: {}
        }
    }

    config = {
        navigationBarTitleText: '歌单'
    }

    async componentDidMount() {
        let query = {
            id: this.$router.params.id
            // id:32659890
        }
        let { data } = await API.movieModel.GetMovieDetail(query)
        console.log(data.playlist)
        this.setState({
            musicInfo: data.playlist
        })

    }

    componentWillReceiveProps(nextProps) {
        console.log(this.props, nextProps)
    }



    render() {
        const { musicInfo } = this.state
        if (musicInfo.coverImgUrl) {
            var headStyle = {
                background: `url(${musicInfo.coverImgUrl})`,
                zIndex: '-2',
                position: 'absolute',
                top: 0,
                left: 0,
                right: 0,
                bottom: 0,
                filter: 'blur(20px)',
            }
        }
        return (
            <View className={styles.detail_wrapper} >
                {
                    Object.keys(musicInfo).length ?
                        <View className={styles.detail_container}>
                            <View className={styles.detail_header}>
                                <View style={headStyle}></View>
                                <View className={styles.detail_header_info}>
                                    <View className={styles.sub_info_cover}>
                                        <image src={musicInfo.coverImgUrl}></image>
                                    </View>
                                    <View className={styles.sub_info_content}>
                                        <Text style={{ fontSize: "16px" }}>{musicInfo.name}</Text>
                                        <View className={styles.sub_info_creator}>
                                            <image src={musicInfo.creator.avatarUrl}></image>
                                            <Text style={{ fontSize: "14px",marginLeft:"10px" }}>{`${musicInfo.creator.nickname}`}</Text>
                                        </View>
                                    </View>
                                </View>
                            </View>
                            <View style={{borderRadius:"100px"}}>
                                <PlayMusicList musicList={musicInfo.tracks} total={musicInfo.tracks.length} subscribedCount={countFilter(musicInfo.subscribedCount)}></PlayMusicList>
                            </View>
                        </View>
                        : ''
                }
            </View>
        )
    }
}

export default movieDetail
