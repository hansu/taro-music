import Taro, { Component,useState } from '@tarojs/taro'
import { View, Button, Text, Swiper, SwiperItem, ScrollView } from '@tarojs/components'
import { connect } from '@tarojs/redux'

import API from "@/apis"
import { GetBannerListAction,GetRecommendListAction} from '@/store/actions/music'
import MyBanner from "@/components/common/banner"
import MyRecommend from "@/components/common/recommend"
// import MovieList from "./components/movieList"
import styles from './home.module.scss'
import { AtIcon } from '_taro-ui@2.2.2@taro-ui';


@connect(({ music:{bannerList,recommendList} }) => ({
  bannerList,
  recommendList
}), (dispatch) => ({
  asyncGetBannerList() {
    dispatch(GetBannerListAction())
  },
  asyncGetRecommendList() {
    dispatch(GetRecommendListAction())
  }
}))
class Index extends Component {

  constructor(props) {
    super(props)
  }

  config = {
    navigationBarTitleText: '网易云音乐'
  }

  showDetail = (type) => {
    switch(type){
      case "4":Taro.navigateTo({url:'/pages/rank/index'})
    }
  }

  componentWillUnmount() { }

  componentDidMount() {
    if(!this.props.bannerList.length){
      this.props.asyncGetBannerList()
    }
    if(!this.props.recommendList.length){
      this.props.asyncGetRecommendList()
    }
  }

  render() {
    return (
      <View className={styles.home_wrapper}>
        <MyBanner list={this.props.bannerList}></MyBanner>
        <View className={styles.home_cate_wrapper}>
          <View className={styles.home_cate_item}>
            <View className={styles.home_cate_item_img}>
                <AtIcon value="calendar" color="#fff"></AtIcon>
            </View>
            <Text style={{marginTop:"10px"}}>
              每日推荐
            </Text>
          </View>

          <View className={styles.home_cate_item}>
            <View className={styles.home_cate_item_img}>
                <AtIcon value="file-audio" color="#fff"></AtIcon>
            </View>
            <Text style={{marginTop:"10px"}}>
              歌单
            </Text>
          </View>

          <View className={styles.home_cate_item}>
            <View className={styles.home_cate_item_img}>
                <AtIcon value="sound" color="#fff"></AtIcon>
            </View>
            <Text style={{marginTop:"10px"}}>
              电台
            </Text>
          </View>

          <View className={styles.home_cate_item} onClick={()=>{this.showDetail("4")}}>
            <View className={styles.home_cate_item_img}>
                <AtIcon value="align-center" color="#fff"></AtIcon>
            </View>
            <Text style={{marginTop:"10px"}}>
              排行榜
            </Text>
          </View>

          <View className={styles.home_cate_item}>
            <View className={styles.home_cate_item_img}>
                <AtIcon value="video" color="#fff"></AtIcon>
            </View>
            <Text style={{marginTop:"10px"}}>
              直播
            </Text>
          </View>
        </View>
        <MyRecommend movieList={this.props.recommendList}></MyRecommend>
      </View>
    )
  }
}

export default Index
