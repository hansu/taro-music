import Taro, { Component } from '@tarojs/taro'
import { View, Text } from '@tarojs/components'
import MyIcon from "@/components/common/myIcon"
import styles from "./index.module.scss";
import API from "@/apis"
const BAM = Taro.getBackgroundAudioManager()

class MyPlayer extends Component {

  config = {
    navigationBarTitleText: '歌单'
  }

  constructor(props) {
    super(props)
    this.state = {
      isCurPlay:false,
      music: {},
      playIcon: 'play'
    }
  }

  async componentWillMount() {
    let id = this.$router.params.id
    let { data: { songs } } = await API.movieModel.GetSongDetail({ id })
    let { data } = await API.movieModel.GetSongUrl({ id })
    this.setState({
      music: songs[0],
      musicUrl: data[0].url
    }, () => {
      Taro.setNavigationBarTitle({
        title: this.state.music.al.name
      })
      this.handlePlay()
      BAM.onPlay(() => {
        console.log("play")
      })
    })
  }


  handlePlay = () => {
    if (!this.state.isCurPlay) {
      if(this.state.musicUrl){
        this.setState({
          isCurPlay:true,
          playIcon: 'pause'
        }, () => {
          if(this.state.musicUrl){
            BAM.src = this.state.musicUrl
            BAM.title = this.state.music.name
            BAM.singer = this.state.music.al.name
            BAM.coverImgUrl = this.state.music.picUrl
            BAM.play()
          }
        })
      }
      else{
        Taro.showToast({
          title:"需要解锁会员权限",
          icon:"none"
        }) 
      }
    }
    else {
      this.setState({
        isCurPlay:false,
        playIcon: 'play'
      }, () => {
        BAM.pause()
      })
    }
  }


  render() {
    let song_disc = {}
    if (Object.keys(this.state.music).length) {
      song_disc = {
        background: 'url(' + this.state.music.al.picUrl + ')',
        backgroundSize: 'cover',
        position: "absolute",
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        zIndex: -1,
        backgroundColor: '#161824',
        backgroundPosition: '50%'
      }
    }
    return (
      <View>
        {Object.keys(this.state.music).length ? <View className={styles.song_disc}>
          <View style={song_disc}></View>
          <View className={styles.song_cover_container} onClick={this.handlePlay}>
            <View className={styles.song_cover_content}>
              <View className={styles.song_record}>
                <View className={[styles.song_record_cover, this.state.isCurPlay ? styles.move : '']}>
                  <image src={music.al.picUrl}></image>
                </View>
                <View className={styles.play_icon}>
                  <MyIcon value={this.state.playIcon} size="30" color="#fff"></MyIcon>
                </View>
              </View>
            </View>
          </View>
        </View> : ''
        }
      </View>
    )
  }
}

export default MyPlayer
