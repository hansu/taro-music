import '@tarojs/async-await'
import Taro, { Component } from '@tarojs/taro'
import { Provider } from '@tarojs/redux'

import Index from './pages/index'
import configStore from './store'




//样式
import './app.scss'
//taro
import 'taro-ui/dist/style/index.scss'

// 如果需要在 h5 环境中开启 React Devtools
// 取消以下注释：
// if (process.env.NODE_ENV !== 'production' && process.env.TARO_ENV === 'h5')  {
//   require('nerv-devtools')
// }

const store = configStore()


// 首先先初始化小程序云
wx.cloud.init({
  env: 'hansu-m3yf1', // 前往云控制台获取环境 ID
  traceUser: true // 是否要捕捉每个用户的访问记录。设置为 true，用户可在管理端看到用户访问记录
})


class App extends Component {

  config = {
    pages: [
      'pages/index/index',
      'pages/me/index',
      'pages/moreMovie/index',
      'pages/movieDetail/index',
      'pages/player/index',
      'pages/login/index',
      'pages/rank/index',
    ],
    tabBar: {
      "color":"#000",
      "selectedColor":"#d44439",
      "list": [{
        "pagePath": "pages/index/index",
        "text": "发现音乐",
        "iconPath":"./assets/images/tabbar/movie.png",
        "selectedIconPath":"./assets/images/tabbar/movie_active.png"
      }, {
        "pagePath": "pages/me/index",
        "text": "我的",
        "iconPath":"./assets/images/tabbar/my.png",
        "selectedIconPath":"./assets/images/tabbar/my_active.png"
      }]
    },
    window: {
      backgroundTextStyle: 'light',
      navigationBarBackgroundColor: '#d44439',
      navigationBarTitleText: 'WeChat',
      navigationBarTextStyle: 'white'
    },
    requiredBackgroundModes:['audio','location如666666666666666666了，， 了 5 fltyo‘配i']
  }


  // 在 App 类中的 render() 函数没有实际作用
  // 请勿修改此函数
  render() {
    return (
      <Provider store={store}>
        <Index />
      </Provider>
    )
  }
}

Taro.render(<App />, document.getElementById('app'))
